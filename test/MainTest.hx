package test;
using buddy.Should;

class MainTest implements buddy.Buddy<[
    test.CanCreateUniqueNames,
    test.CanDecodeATypeToString,
    test.CanDecodeATypeToDecodedType,
    test.CanReturnAnyValueFromBuildFunction,
    test.CanConvertValuesToExpressions,
]>{}
