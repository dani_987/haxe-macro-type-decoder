package test.helper;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr.ComplexType;

using hx.macro.MacroTypeHelper;
using hx.macro.MacroTypeDecorderHelper;
using hx.macro.MacroDefineHelper;

using haxe.macro.ExprTools;

class MacroTesterBuilder {
    static function build() : ComplexType {
		switch (Context.getLocalType()) {
			case TInst(_.get() => {name: name}, []):
                var newName = '${name}_build';
                var path = ["test","helper"];

                var recursiveStruct = (macro : test.helper.MacroTester.RecursiveIntStruct).toType().decode();

                if(! newName.isDefinedAsTypeInPath(path)){
                    var someList : List<Int> = new List();
                    someList.add(2);
                    someList.add(7);
                    someList.add(5);

                    Context.defineType({
                        pack: path,
                        name: newName,
                        pos: MacroDefineHelper.getPosition(),
                        params: [],
                        kind: TDClass(),
                        fields: [
                            "new".toPublicFunction([], macro {}, macro: Void),
                            "getSomeList".toPublicConstValueFunction(
                                someList, macro : List<Int>
                            ),
                            "getSomeMap".toPublicConstValueFunction(
                                [
                                    "hi" => 5,
                                    "hallo" => 7,
                                ], macro : Map<String,Int>
                            ),
                            "getInt".toPublicFunction([], macro {return ${5.asTypedExpr(macro : Int)};}, macro : Int),
                            "getRecursiveStruct".toPublicFunction(
                                [],
                                macro {return ${recursiveStruct.asTypedExpr(macro : hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType)};},
                                macro : hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType
                            ),
                        ],
                    });
                }

                return TPath({pack: path, name: newName, params: []});

			default:
				throw 'Illegal Type ${Context.getLocalType()}!';
		}
    }
}
#end
