package test.helper;

typedef RecursiveIntStruct = {
    someData : Int,
    next : RecursiveIntStruct,
}
@:genericBuild(test.helper.MacroTesterBuilder.build())
class MacroTester{}
