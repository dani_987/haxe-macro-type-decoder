package test;
import hx.macro.MacroTypeDecorderHelper.DecodedTypeEnum;
import buddy.BuddySuite;
using buddy.Should;
import test.helper.MacroTester;

class CanConvertValuesToExpressions extends BuddySuite {
    public function new() {
        describe("Converting Values to Expressions", {
            var builder = new MacroTester();
            it("should work with Basic Types", {
                builder.getInt().should.be(5);
            });
            it("should work with recursive data", {
                var data = builder.getRecursiveStruct();
                switch(data.decoded){
                    case TypeStruct(fields):
                        var nextField = fields.filter(f->f.name=="next").shift();
                        nextField.should.not.be(null);
                        switch(nextField.decoded){
                            case TypeStruct(flds):
                                flds.should.be(fields);
                            case _: fail('Should be TypeStruct');
                        }
                    case _: fail('Should be TypeStruct');
                }
            });
        });
    }
}
