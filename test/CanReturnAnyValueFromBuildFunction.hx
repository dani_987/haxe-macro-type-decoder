package test;
import test.helper.MacroTester;
import buddy.BuddySuite;

using buddy.Should;

class CanReturnAnyValueFromBuildFunction extends BuddySuite {

    public function new() {
        describe("While building a type", {
            it("a function returning a list can be build", {
                var someList = new MacroTester().getSomeList();
                someList.pop().should.be(2);
                someList.pop().should.be(7);
                someList.pop().should.be(5);
            });
            it("a function returning a map can be build", {
                var someMap = new MacroTester().getSomeMap();
                someMap["hi"].should.be(5);
                someMap["hallo"].should.be(7);
            });
        });
    }
}
