package test;
import hx.macro.TypeToStringPrinter;
import buddy.BuddySuite;
import test.CanDecodeATypeToString.SomeStruct;

using buddy.Should;

enum SomeEnum<T>{
    Val1;
    Val2(val: T);
}

class CanDecodeATypeToDecodedType extends BuddySuite {

    public function new() {
        describe("Decoding a type as runtime type", {
            it("should return TypeInt", {
                var result = new TypeToStringPrinter<Int>().getDecodedData().decoded;
                switch(result){
                    case TypeInt:
                    default:
                        trace(result);
                        fail('Unexpected return type $result');
                }
            });
            it("should return a corrct TypeStruct", {
                var result = new TypeToStringPrinter<{bla:String,?foo:Array<Int>}>().getDecodedData().decoded;
                switch(result){
                    case TypeStruct([
                        {name:"bla", decoded:TypeString},
                        {name:"foo", nullable:true, decoded:TypeArray({decoded:TypeInt})},
                    ]):
                    default:
                        trace(result);
                        fail('Unexpected return type $result');
                }
            });
            it("should return a correct TypeStruct for SomeStruct", {
                var result = new TypeToStringPrinter<SomeStruct>().getDecodedData().decoded;
                switch(result){
                    case TypeStruct([
                        {name:"bl", decoded:TypeBool},
                        {name:"fl", decoded:TypeFloat},
                        {name:"list", decoded:TypeList({decoded:TypeInt})},
                        {name:"map", decoded:TypeMap({decoded:TypeInt}, {decoded:TypeString})},
                    ]):
                    default:
                        trace(result);
                        fail('Unexpected return type $result');
                }
            });
            it("should return a TypeEnum", {
                var result = new TypeToStringPrinter<SomeEnum<Int>>().getDecodedData().decoded;
                switch(result){
                    case TypeEnum(posibilities):
                        posibilities.exists("Val1").should.be(true);
                        posibilities.exists("Val2").should.be(true);
                        posibilities["Val1"].length.should.be(0);
                        posibilities["Val2"].length.should.be(1);
                    default:
                        trace(result);
                        fail('Unexpected return type $result');
                }
            });
        });
    }
}
