package test;
import hx.macro.MacroTypeHelper;
import buddy.BuddySuite;

using buddy.Should;

class CanCreateUniqueNames extends BuddySuite {

    public function new() {
        describe("IdentifierNameBuilder", {
            it("should build unique names", {
                var builder = MacroTypeHelper.createNameBuilder("test");
                builder.next().get().should.be("test_1");
                builder.next().get().should.be("test_2");
            });
        });
    }
}
