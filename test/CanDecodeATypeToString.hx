package test;
import hx.macro.TypeToStringPrinter;
import buddy.BuddySuite;
using buddy.Should;

typedef SomeStruct = {
    fl: Float,
    map: Map<Int, String>,
    list: List<Int>,
    bl: Bool,
}
typedef TypedStruct<T> = {
    > SomeStruct,
    data: T,
    name: String,
}

class CanDecodeATypeToString extends BuddySuite {

    public function new() {
        describe("Decoding a type as string", {
            it("should return a correct String for Int", {
                new TypeToStringPrinter<Int>().getString().should.be("Int");
            });
            it("should return a correct String for an anonymous struct with optional", {
                new TypeToStringPrinter<{bla:String,?foo:Array<Int>}>().getString().should.be("{bla:String,foo:Null<Array<Int>>}");
            });
            it("should return a correct String for a struct", {
                new TypeToStringPrinter<SomeStruct>().getString().should.be("SomeStruct");
            });
            it("should return a correct again the same String for a struct", {
                new TypeToStringPrinter<SomeStruct>().getString().should.be("SomeStruct");
            });
            it("should return a correct String for a typed struct", {
                new TypeToStringPrinter<TypedStruct<Int>>().getString().should.be("TypedStruct<Int>");
            });
        });
    }
}
