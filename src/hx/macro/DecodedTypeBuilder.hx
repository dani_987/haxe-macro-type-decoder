package hx.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

using hx.macro.MacroTypeDecorderHelper;
using hx.macro.MacroDefineHelper;
using haxe.macro.Tools;

class DecodedTypeBuilder {
    static function build() : ComplexType {
        switch (Context.getLocalType()) {
            case TInst(_.get() => {name: name, pack: pack}, params):
                return buildFunctions(pack, name, params);
            default:
                throw 'Illegal Type ${Context.getLocalType()}!';
        }
    }

    private static function buildFunctions(
        pack: Array<String>, name: String, params:Array<Type>
    ) : ComplexType{
        var numParams = params.length;
        var newName = '${name}__${params.map((param) -> {param.toUniqueName();}).join('_I_')}';

        if (!MacroTypeDecorderHelper.isDefinedAsTypeInPath(newName, pack)) {
			Context.defineType({
				pack: pack,
				name: newName,
				pos: MacroDefineHelper.getPosition(),
				params: [for (i in 0...numParams){name: 'T$i'}],
				kind: TDClass(),
				fields: getFields(params),
			});
		}
        return TPath({pack: pack, name: newName, params: [for (t in params) TPType(t.toComplexType())]});
    }

    public static function getFields(paramTypes:Array<Type>) : Array<Field> {
        if(paramTypes.length == 0)throw "Missing Parameter";
        if(paramTypes.length > 1)throw "Only 1 Parameter is allowed";
        var type = macro: hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType;
        var decoded = paramTypes[0].decode();
        var typeDef : Expr = decoded.asTypedExpr(macro : hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType);
        var fields : Array<Field> = [
            {
                name: "decoded",
                access: [APrivate,AStatic],
                pos: Context.currentPos(),
                kind: FVar(type, typeDef)
            },
            "new".toPublicFunction([], macro {}, macro : Void),
            "get".toPublicFunction([], macro {return decoded;}, type),
        ];
		return fields;
    }
}
#end
