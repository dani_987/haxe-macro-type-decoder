package hx.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;

using hx.macro.MacroTypeDecorderHelper;
using hx.macro.MacroDefineHelper;

using haxe.macro.ExprTools;

class TypeToStringPrinterMacro {
    static function build() : ComplexType {
		switch (Context.getLocalType()) {
			case TInst(_.get() => {name: name}, [type]):
                var newName = '${name}_${type.toUniqueName()}';
                var path = ["hx","macro"];

                if(!newName.isDefinedAsTypeInPath(path)){

                    var typeDecodedForString = type.decodeForOutput().outputToString();
                    var typeString = typeDecodedForString;

                    Context.defineType({
                        pack: path,
                        name: newName,
                        pos: MacroDefineHelper.getPosition(),
                        params: [{name:"T"}],
                        kind: TDClass(),
                        fields: [
                            "new".toPublicFunction([], macro {}, macro: Void),
                            "getString".toPublicConstValueFunction(
                                type.decodeForOutput().outputToString(), macro: String
                            ),
                            "getDecodedData".toPublicConstValueFunction(
                                type.decode(), macro : hx.macro.MacroTypeDecorderHelper.DecodedRuntimeType
                            ),
                        ],
                    });
                }

                return TPath({pack: path, name: newName, params: [type.toTypeParam()]});

			default:
				throw 'Illegal Type ${Context.getLocalType()}!';
		}
    }
}
#end
