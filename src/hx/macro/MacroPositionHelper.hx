package hx.macro;

#if macro
import haxe.PosInfos;
import haxe.macro.Expr.Position;
import sys.io.File;
import sys.io.FileInput;
import haxe.macro.PositionTools;

class MacroPositionHelper {
    private var fileReader: FileInput;
    private var startByte: Int = 0;

    private function new(fileName: String) {
        fileReader = File.read(fileName);
    }

    public static inline function toMacroPosition(pos: PosInfos) : Position {
        try{
            var fileReader = new MacroPositionHelper(pos.fileName);
            var min: Int = 0;
            var max: Int = 0;
            for(_ in 1...pos.lineNumber){
                min += fileReader.countBytesInLine().len;
            }
            var lastLine = fileReader.countBytesInLine();
            max = min + lastLine.len - 1;
            min += lastLine.bytesAtLineStart;
            if(max <= min){
                max = min + 1;
            }
            return PositionTools.make({ file: pos.fileName, min: min, max: max });
        } catch (_:Any){ return (macro null).pos; }
    }

    private function countBytesInLine() : {len: Int, bytesAtLineStart: Int} {
        var lastByte : Int;
        var countBytes : Int = 0;
        var bytesAtLineStart : Int = 0;
        try{
            while(true){
                lastByte = fileReader.readByte();
                countBytes++;
                switch(lastByte)
                {
                    case 10 | 13:
                        if(startByte == lastByte || countBytes != 1){
                            startByte = lastByte;
                            return {len: countBytes, bytesAtLineStart: bytesAtLineStart};
                        } else {
                            bytesAtLineStart++;
                        }
                    default:
                }
            }
        } catch (_:Any){ return {len: countBytes, bytesAtLineStart: bytesAtLineStart}; }
    }
}
#end
