package hx.macro;

import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.TypeTools;

using haxe.macro.ComplexTypeTools;
#if macro
import haxe.macro.Context;

using hx.macro.MacroDefineHelper;
#end

#if macro
typedef DecodedType = {
    realType: Type,
    decoded: DecodedTypeEnum<DecodedType,Parameter>,
    nullable: Bool,
    typeName: String,
}
#else
typedef DecodedType = DecodedRuntimeType;
#end

typedef DecodedRuntimeType = {
    decoded: DecodedTypeEnum<DecodedRuntimeType,RuntimeParameter>,
    nullable: Bool,
    typeName: String,
}

typedef Parameter = {
    > DecodedType,
    name: String,
}
typedef RuntimeParameter = {
    > DecodedRuntimeType,
    name: String,
}

enum DecodedTypeEnum<T,P> {
    TypeInt;
    TypeFloat;
    TypeString;
    TypeBool;
    TypeArray(of:T);
    TypeList(of:T);
    TypeMap(ofKey:T,ofValue:T);
    TypeStruct(fields: Array<P>);
    TypeEnum(posibilities: Map<String, Array<T>>);
}

typedef DecodedTypeForOutput = {
    name: String,
    params: Array<DecodedTypeForOutput>,
}

#if macro
enum InitExpr {
    SingleValue(val: Expr);
    ArrayValue(vals: Array<Expr>);
    ObjectValue(vals: Map<String, Expr>);
}
#end

class MacroTypeDecorderHelper {
    #if macro
    private static var decodedValues : Map<String, DecodedType> = new Map();

    /** Checks if a type is defined */
    public static function isDefinedAsTypeInPath(name: String, ?pack: Array<String>) : Bool{
        var fullName = name;
        if(pack != null && pack.length > 0){
            fullName = '${pack.join(".")}.$fullName';
        }
        try{
            Context.getType(fullName);
            return true;
        }catch(_){return false;}
    }

    /** creates code, that will return the given value */
    public static function asReturnExpr(toReturn: Dynamic) : Expr {
        var result = MacroValueSaverExpr.saveToVariable(
            toReturn,
            "result"
        );
        result.push( macro return result );
        return result.toExprBlock();
    }

    public static function asExpr(toReturn: Dynamic) : Expr {
        var body = asReturnExpr(toReturn);
        return macro ((()->$body)());
    }

    /** creates code, that will return the given value - ComplexType can be got: macro : TypeName */
    public static function asTypedReturnExpr(toReturn: Dynamic, typed: ComplexType) : Expr {
        var result = MacroValueSaverExpr.saveToTypedVariable(
            toReturn,
            "result",
            typed
        );
        result.push( macro return result );
        return result.toExprBlock();
    }

    /** ComplexType can be got: macro : TypeName */
    public static function asTypedExpr(toReturn: Dynamic, typed: ComplexType) : Expr {
        var body = asTypedReturnExpr(toReturn, typed);
        return macro ((()->$body)());
    }
    #end

    public static function toTypeParam(from: Type) : TypeParam {
        return TPType(TypeTools.toComplexType(from));
    }

    public static function outputToString(output: DecodedTypeForOutput) : String {
        return '${output.name}${if(output.params.length > 0) '<${output.params.map((param)->{
            outputToString(param);
        }).join(",")}>' else ""}';
    }

    public static function decodeForOutput(ofType: Type) : DecodedTypeForOutput {
        switch(ofType){
            case null:
                return {name: "null", params: []};
            case TMono(_.get() => type):
                return decodeForOutput(type);
            case TEnum(_.get() => type, params):
                return {name: type.name, params: params.map((param) -> {decodeForOutput(param);})};
            case TInst(_.get() => type, params):
                return {name: type.name, params: params.map((param) -> {decodeForOutput(param);})};
            case TType(_.get() => type, params):
                return {name: type.name, params: params.map((param) -> {decodeForOutput(param);})};
            case TFun(args, ret):
                return {name: '(${
                    args.map( (arg)->{ outputToString(decodeForOutput(arg.t)); }).join(",")
                })->${
                    outputToString(decodeForOutput(ret))
                }', params: []};
            case TAnonymous(_.get() => anonym):
                return {name: '{${anonym.fields.map(
                    (field)->{ '${field.name}:' + outputToString(decodeForOutput(field.type)); }
                ).join(",")}}', params: []};
            case TDynamic(type):
                if(type == null) return {name: "Dynamic", params: []};
                else return {name: "Dynamic", params: [decodeForOutput(type)]};
            case TLazy(f):
                return {name: "?", params: []};
            case TAbstract(_.get() => type, params):
                return {name: type.name, params: params.map((param) -> {decodeForOutput(param);})};
        }
    }

    private static function getUnsupportedErrorMessage(unsupported: Type) : String {
        return 'Type "${outputToString(decodeForOutput(unsupported))}" is not supported (${unsupported.getName()})';
    }

	#if macro
    public static function toNullableType(nonNullableType: Type) : Type {
        switch((macro : Null<Int>).toType()){
            case TAbstract(nullType, _):
                return TAbstract(nullType, [nonNullableType]);
            default: throw "Should never happen";
        }
    }

    public static function decode(typeToDecode: Type) : DecodedType {
        var result = decode_intern(typeToDecode);
        if(result.nullable){
            result.realType = toNullableType(result.realType);
        }
        return result;
    }

    private static function decode_intern(typeToDecode: Type) : DecodedType {
        var typeString = outputToString(decodeForOutput(typeToDecode));
        var typeName = ComplexTypeTools.toString(TypeTools.toComplexType(typeToDecode));
        if(decodedValues.exists( typeString )){
            return decodedValues[typeString];
        }
        switch(typeToDecode){
            case TAbstract(_.get() => {module: "StdTypes", name: "Null", params:[param]}, [subType]):
                var result = decode(subType);
                result.nullable = true;
                return result;
            case TType(_.get() => typeDef,params):
                var realTypeDef = TypeTools.applyTypeParameters(typeDef.type, typeDef.params, params);
                var result = decode(realTypeDef);
                result.nullable = typeDef.meta.has(":optional");
                return result;
            case TAbstract(_.get() => {meta: meta, module: "StdTypes", name: "Bool"}, _):
                return {realType: typeToDecode, decoded: TypeBool, nullable: meta.has(":optional"), typeName: typeName};
            case TAbstract(_.get() => {meta: meta, module: "StdTypes", name: "Int"}, _):
                return {realType: typeToDecode, decoded: TypeInt, nullable: meta.has(":optional"), typeName: typeName};
            case TAbstract(_.get() => {meta: meta, module: "StdTypes", name: "Float"}, _):
                return {realType: typeToDecode, decoded: TypeFloat, nullable: meta.has(":optional"), typeName: typeName};
            case TAbstract(_.get() => {meta: meta, module: "haxe.ds.Map", name: "Map", params:[_,_]}, [p1,p2]):
                return {realType: typeToDecode, decoded: TypeMap(decode(p1),decode(p2)), nullable: meta.has(":optional"), typeName: typeName};
            case TInst(_.get() => {meta: meta, module: "Array", name: "Array"}, [param]):
                var ofType : DecodedType = cast {};
                var result = {realType: typeToDecode, decoded: TypeArray(ofType), nullable: meta.has(":optional"), typeName: typeName};
                decodedValues[typeString] = result;
                var decoded = decode(param);
                ofType.realType = decoded.realType;
                ofType.decoded = decoded.decoded;
                ofType.nullable = decoded.nullable;
                ofType.typeName = decoded.typeName;
                return result;
            case TInst(_.get() => {meta: meta, module: "haxe.ds.List", name: "List"}, [param]):
                var ofType : DecodedType = cast {};
                var result = {realType: typeToDecode, decoded: TypeList(ofType), nullable: meta.has(":optional"), typeName: typeName};
                decodedValues[typeString] = result;
                var decoded = decode(param);
                ofType.realType = decoded.realType;
                ofType.decoded = decoded.decoded;
                ofType.nullable = decoded.nullable;
                ofType.typeName = decoded.typeName;
                return result;
            case TInst(_.get() => {meta: meta, module: "String", name: "String"}, _):
                return {realType: typeToDecode, decoded: TypeString, nullable: meta.has(":optional"), typeName: typeName};
            case TAnonymous(_.get() => anony):
                var arr = [];
                var result = {realType: typeToDecode, decoded: TypeStruct(arr), nullable: false, typeName: typeName};
                decodedValues[typeString] = result;
                for(field in anony.fields){
                    var decoded = decode(field.type);
                    arr.push({
                        realType: decoded.realType,
                        decoded: decoded.decoded,
                        nullable: decoded.nullable || field.meta.has(":optional"),
                        name: field.name,
                        typeName: decoded.typeName
                    });
                };
                return result;
            case TEnum(_.get() => enumVal, params):
                var posibilities: Map<String, Array<DecodedType>> = new Map();
                var result = {realType: typeToDecode, decoded: TypeEnum(posibilities), nullable: enumVal.meta.has(":optional"), typeName: typeName};
                decodedValues[typeString] = result;
                for(field in enumVal.constructs){
                    switch(field.type){
                        case TFun(args, _):
                            posibilities[field.name] = args.map( (val) -> {
                                var realType = TypeTools.applyTypeParameters(val.t, enumVal.params, params);
                                return decode(realType);
                            } );
                        case TEnum(_, _): posibilities[field.name] = [];
                        default:
                            trace(field.type);
                            throw getUnsupportedErrorMessage(field.type);
                    }
                }
                return result;
            case TLazy(f):
                return decode(f());
            default:
                throw getUnsupportedErrorMessage(typeToDecode);
        }
    }
    #end

    //create a unique name for a type, that can be used as a part of a varname
    public static function toUniqueName(
        typeToDecode: Type,
		komma: String = "_I_",
		open: String = "_C_",
		close: String = "_D_",
		equals: String = "_E_",
    ) : String {
        return getTypeNames([typeToDecode], komma, open, close, equals);
    }
	private static function getTypeNames(
        params: Array<Type>, komma: String, open: String, close: String, equals: String,
	) : String {
		return params.map((param) -> {switch(param) {
            case TAbstract(_.get() => type,[]): type.name;
			case TAbstract(_.get() => type,subParams):
                type.name + open + getTypeNames(subParams, komma, open, close, equals) + close;
            case TInst(_.get() => type,[]): type.name;
            case TInst(_.get() => type,subParams):
                type.name + open + getTypeNames(subParams, komma, open, close, equals) + close;
            case TType(_.get() => type,[]): type.name;
			case TType(_.get() => type,subParams):
				type.name + open + getTypeNames(subParams, komma, open, close, equals) + close;
			case TAnonymous(_.get() => anonym):
				open + anonym.fields.map((field) -> {
					field.name + equals + getTypeNames([field.type], komma, open, close, equals);
				}).join(komma) + close;
            case TEnum(_.get() => enumVal, []): enumVal.name;
            case TEnum(_.get() => enumVal, subParams):
                enumVal.name + open + getTypeNames(subParams, komma, open, close, equals) + close;
            case TLazy(f): getTypeNames([f()], komma, open, close, equals);
			case _: throw 'Illegal Type "$param" at position ${param.getIndex()}';
		} }).join(komma);
	}

    public static function toRealUniqueName(
        typeToDecode: DecodedType,
		komma: String = "_I_",
		open: String = "_C_",
		close: String = "_D_",
		equals: String = "_E_",
    ) : String {
        return getRealTypeNames([typeToDecode], komma, open, close, equals);
    }

    private static function isNullable(type: DecodedType) : String {
        if(type.nullable) return "__OPT";
        return "";
    }

    private static function joinIter<T>(iter: Iterator<T>, merge: (T,T)->T) : Null<T> {
        var result : Null<T> = null;
        for(val in iter){
            if(result==null)result=val;
            else result=merge(result,val);
        }
        return result;
    }

	private static function getRealTypeNames(
        params: Array<DecodedType>, komma: String, open: String, close: String, equals: String,
	) : String {
		return params.map((param) -> {switch(param.decoded) {
            case TypeInt: "Int" + isNullable(param);
            case TypeFloat: "Float" + isNullable(param);
            case TypeString: "String" + isNullable(param);
            case TypeBool: "Bool" + isNullable(param);
            case TypeArray(of): "Array" + open + getRealTypeNames([of], komma, open, close, equals) + close + isNullable(param);
            case TypeList(of): "List" + open + getRealTypeNames([of], komma, open, close, equals) + close + isNullable(param);
            case TypeMap(ofKey,ofVal): "Map" + open + getRealTypeNames([ofKey,ofVal], komma, open, close, equals) + close + isNullable(param);
            case TypeStruct(fields):
				open + fields.map((field) -> {
					field.name + equals + getRealTypeNames([field], komma, open, close, equals) + isNullable(param);
				}).join(komma) + close;
            case TypeEnum(posibilities):
                "ENUM" + open + joinIter(posibilities.keys(), (v1,v2)->{v1 + komma + v2;}) + close + isNullable(param);
			case _: throw 'Illegal Type "$param"';
		} }).join(komma);
	}
}
