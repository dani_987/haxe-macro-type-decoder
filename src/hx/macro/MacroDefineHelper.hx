package hx.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.PosInfos;

using hx.macro.MacroPositionHelper;

enum PositionDefinition {
	InMacro;
	OnMacroCall;
}

class MacroDefineHelper {
	private static function buildAcess(isPublic: Bool, isStatic: Bool) : Array<Access> {
		var res = [if(isPublic) APublic else APrivate];
		if(isStatic)res.push(AStatic);
		return res;
	}
	private static function newFunction(name: String, isPublic: Bool, isStatic: Bool, args: Array<FunctionArg>, ret: ComplexType, expr: Expr) : Field {
		return {
			name: name,
			access: buildAcess(isPublic, isStatic),
			pos: expr.pos,
			kind: FFun({
				args: args,
				ret: ret,
				expr: expr
			})
		};
	}

	public static function toPublicFunction(name: String, args: Array<FunctionArg>, expr: Expr, ret: ComplexType) : Field {
		return newFunction(name, true, false, args, ret, expr);
	}

	public static function toPublicConstValueFunction(name: String, val: Dynamic, type: ComplexType) : Field {
		return toPublicFunction(name, [], MacroTypeDecorderHelper.asTypedReturnExpr(val, type), type);
	}


	public static function toPrivateFunction(name: String, args: Array<FunctionArg>, expr: Expr, ret: ComplexType) : Field {
		return newFunction(name, false, false, args, ret, expr);
	}

	public static function toPrivateStaticFunction(name: String, args: Array<FunctionArg>, expr: Expr, ret: ComplexType) : Field {
		return newFunction(name, false, true, args, ret, expr);
	}
	public static function toStaticFunction(name: String, args: Array<FunctionArg>, expr: Expr, ret: ComplexType) : Field {
		return newFunction(name, true, true, args, ret, expr);
	}

	public static function toVariable(name: String, type: ComplexType, isPublic: Bool = false, initValue: Null<Expr> = null) : Field {
		return {
			name: name,
			access: [if(isPublic) APublic else APrivate],
			pos: Context.currentPos(),
			kind: FVar(type, initValue)
		};
	}

	/** Returns the Position, if
		 - InMacro is passed, the position of the function inside the macro will be retured (default)
		 - OnMacroCall is passed, the position of the function is passed, that has called the macro
	*/
	public static inline function getPosition(of: PositionDefinition = InMacro, ?pos: PosInfos) : Position {
		switch(of){
			case InMacro: return pos.toMacroPosition();
			case OnMacroCall: return Context.currentPos();
		}
	}

	public static inline function toExpr(expr: ExprDef, of: PositionDefinition = InMacro, ?pos: PosInfos) : Expr {
		return {
			expr: expr,
			pos: getPosition(of, pos)
		}
	}

	public static inline function toExprBlock(exprs: Array<Expr>, of: PositionDefinition = OnMacroCall) : Expr {
		return toExpr(EBlock(exprs), of);
	}
}
#end
