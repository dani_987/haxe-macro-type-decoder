package hx.macro;

import haxe.macro.TypeTools;
import haxe.macro.ExprTools;
import haxe.Int64;
#if macro
import haxe.macro.Expr;
import haxe.macro.Context;
import haxe.macro.ComplexTypeTools;

using hx.macro.MacroDefineHelper;
using hx.macro.MacroTypeDecorderHelper;
using StringTools;

class StaticFunctionDefiner {
    private static var identfierMaps : Map<String,IdentifierFromTypeNameBuilder> = [];
    private static var toDefine: List<{
        tdesc: String,
        fun:(StaticFunctionDefiner)->Array<Field>,
        id: IdentifierName,
        definer: StaticFunctionDefiner,
        pos:haxe.PosInfos,
    }> = new List();
    private static var isDefining : Bool = false;
    private var path : Array<String>;
    private var nameGenerator : IdentifierFromTypeNameBuilder;
    private var fieldsGen: (DecodedType,StaticFunctionDefiner)->Array<Field>;
    public function new(path: Array<String>, baseName: String, fieldsGen: (DecodedType,StaticFunctionDefiner)->Array<Field>) {
        this.path = path;
        this.nameGenerator = getNameGenerator(baseName);
        this.fieldsGen = fieldsGen;
    }
    private static inline function getNameGenerator(baseName: String) : IdentifierFromTypeNameBuilder {
        if(!identfierMaps.exists(baseName))identfierMaps[baseName] = new IdentifierFromTypeNameBuilder(baseName);
        return identfierMaps[baseName];
    }

    private static inline function startDecoding() {
        var definer:Null<{tdesc: String, fun:StaticFunctionDefiner -> Array<Field>, id:IdentifierName, definer:StaticFunctionDefiner, pos:haxe.PosInfos}>;
        while((definer = toDefine.pop()) != null){
            var fields = definer.fun(definer.definer);
            //trace(definer.tdesc, definer.id.get());
            Context.defineType({
                pack: definer.definer.path,
                name: definer.id.get(),
                pos: MacroDefineHelper.getPosition(InMacro, definer.pos),
                params: [],
                kind: TDClass(),
                fields: fields
            });
        }
        isDefining = false;
    }

    private inline function defineFunctionForType( fromType: DecodedType, newClass:IdentifierName, pos:haxe.PosInfos ) {
        if(!nameGenerator.isNew(newClass))return;
        toDefine.push({tdesc: TypeTools.toString(fromType.realType), fun: definer->fieldsGen(fromType,definer), definer: this, id: newClass, pos: pos});
        if(isDefining)return;
        isDefining = true;
        startDecoding();
    }

    private function appendPart(from: Null<Expr>, toAppend: String) : Expr {
        if(from == null)return EConst(CIdent(toAppend)).toExpr();
        return EField(from, toAppend).toExpr();
    }

    public function getFunctioncall(fromType: DecodedType, funName: String, ?pos:haxe.PosInfos) : Expr {
        var newClass = nameGenerator.create(fromType);
        defineFunctionForType(fromType, newClass, pos);
        var result : Expr = null;
        for(part in path)result = appendPart(result, part);
        result = appendPart(result, newClass.get());
        var res = appendPart(result, funName);
        return res;
    }
}
#end

class IdentifierName {
    private var name: String;
    public function new(name: String) {
        this.name = name;
    }

    public function get() : String { return name; }

    #if macro
    public function getIdentifier() : ExprDef { return EConst(CIdent(name)); }
    #end
}

class IdentifierNameBuilder {
    private var name: String;
    private var lastId: Int;
    public function new(name: String) {
        this.name = name;
        this.lastId = 0;
    }

    public function next() : IdentifierName {
        lastId++;
        return new IdentifierName('${name}_$lastId');
    }
}

class IdentifierFromTypeNameBuilder {
    private var names: List<{name: String, typename: String, id: Int64}>;
    private var usedNames: List<String>;
    private var name_start: String;
    private var currentId: Int64;
    public function new(name_start: String) {
        this.names = new List();
        this.usedNames = new List();
        this.name_start = name_start;
        currentId = Int64.ofInt(0);
    }

    #if macro
    private function createNewName(typename: String) : IdentifierName {
        currentId = currentId + 1;
        var name = '${name_start}_${currentId.high.hex(8)}${currentId.low.hex(8)}';
        names.push({name:name, id:currentId, typename: typename});
        return new IdentifierName(name);
    }

    public function create(fromType: DecodedType) : IdentifierName {
        var name = names.filter(name->name.typename==fromType.typeName).first();
        if(name == null)return createNewName(fromType.typeName);
        return new IdentifierName(name.name);
    }
    #end

    public function isNew(toTest: IdentifierName) : Bool {
        var nameToTest = toTest.get();
        if(usedNames.filter(it -> it == nameToTest).isEmpty()){
            usedNames.add(nameToTest);
            return true;
        }
        return false;
    }
}

class MacroTypeHelper {
    #if macro
    public static function classToType<T>(fromClass: Class<T>) : haxe.macro.Type {
        return Context.getType(Type.getClassName(fromClass));
    }
    public static function valToType(val: Dynamic) : haxe.macro.Type {
        return classToType(Type.getClass(val));
    }
    public static function toType(fromType: ComplexType) : haxe.macro.Type {
        return ComplexTypeTools.toType(fromType);
    }
    #end

    public static function createNameBuilder(baseName: String) : IdentifierNameBuilder {
        return new IdentifierNameBuilder(baseName);
    }

    public static function createNameFromTypeBuilder(baseName: String) : IdentifierFromTypeNameBuilder {
        return new IdentifierFromTypeNameBuilder(baseName);
    }
}
