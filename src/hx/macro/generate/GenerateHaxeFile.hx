package hx.macro.generate;

#if sys
import haxe.macro.Printer;
import sys.io.File;
import haxe.macro.Expr;
import sys.FileSystem;

using haxe.macro.ExprTools;

class GenerateHaxeFile {
    private var rootDir : String;
    private var pack: Array<String>;
    public function new(rootDir: String, pack: Array<String>) {
        this.rootDir = haxe.io.Path.join([FileSystem.absolutePath(rootDir), haxe.io.Path.join(pack)]);
        this.pack = pack;
        try{ FileSystem.createDirectory(this.rootDir); } catch(e){ trace(e); }
    }

    public function writeClass(filename: String, clazz: ()->TypeDefinition) {
        writeClasses(filename, ()->[clazz()]);
    }

    public function writeClasses(filename: String, classes: ()->Array<TypeDefinition>) {
        var printer = new Printer();
        var fullname = haxe.io.Path.join([rootDir, filename]);
        trace(fullname);
        var file = File.write(fullname, false);
        file.writeString('package ${pack.join(".")};\n');
        for(clazz in classes()){
            file.writeString(printer.printTypeDefinition(clazz, false));
        }
        file.flush();
        file.close();
    }
}
#end
