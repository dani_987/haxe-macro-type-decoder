package hx.macro;

#if macro
import haxe.macro.Expr;

using haxe.macro.TypeTools;
using Type;
using hx.macro.MacroTypeHelper;
using hx.macro.MacroTypeDecorderHelper;
using hx.macro.MacroDefineHelper;
using Reflect;

class MacroValueSaverExpr {
    #if macro
    private var nameBuilder : MacroTypeHelper.IdentifierNameBuilder;

    private function new(rootName: String) {
        this.nameBuilder = MacroTypeHelper.createNameBuilder(rootName);
    }

    public function getExpr(val: Dynamic, varName: String, type: DecodedType, addedBefore:List<{val:Dynamic,name:String}> = null, stack: String = "$") : Array<Expr> {
        var added = new List();
        if(addedBefore != null)added = addedBefore;
        var filtered = added.filter(it->it.val==val);
        if(filtered.length > 0){
            return [EVars([{
                name: varName,
                type: type.realType.toComplexType(),
                expr: EConst(CIdent(filtered.first().name)).toExpr(),
            }]).toExpr()];
        }
        var otherExpr : Array<Expr> = [];
        added.add({val:val, name: varName});
        if(val != null){
            otherExpr.push(EVars([{
                name: varName,
                type: type.realType.toComplexType(),
                expr: null,
            }]).toExpr());
            switch(type.decoded){
                case TypeInt | TypeFloat | TypeString | TypeBool:
                    otherExpr.push(EBinop(OpAssign, EConst(CIdent(varName)).toExpr(), macro $v{val}).toExpr());
                case TypeEnum(params):
                    var enumVal : EnumValue = cast val;
                    var name = enumVal.getName();
                    if(params.exists(name)){
                        var decodedVal = params[name];
                        var resultValue = EConst(CIdent(name)).toExpr();
                        if(decodedVal.length > 0){
                            var pos = 0;
                            resultValue = ECall(EConst(CIdent(name)).toExpr(), decodedVal.map( (param) -> {
                                filtered = added.filter(it->it.val==enumVal.getParameters()[pos]);
                                if(filtered.length > 0)return EConst(CIdent(filtered.first().name)).toExpr();
                                var newVarName = this.nameBuilder.next();
                                otherExpr = otherExpr.concat(this.getExpr(enumVal.getParameters()[pos], newVarName.get(), param, added.filter(it->it.val!=val), '$stack.Enum:$name'));
                                pos++;
                                return newVarName.getIdentifier().toExpr();
                            })).toExpr();
                        }
                        otherExpr.push(EBinop(OpAssign, EConst(CIdent(varName)).toExpr(), resultValue).toExpr());
                    } else {
                        throw '$name is not existent in Enum-TypeDefinition!';
                    }
                case TypeArray(ofType):
                    otherExpr.push(EBinop(OpAssign, EConst(CIdent(varName)).toExpr(), macro []).toExpr());
                    var arrayVal : Array<Dynamic> = cast val;
                    var values : Array<Expr> = [];
                    for(item in arrayVal){
                        filtered = added.filter(it->it.val==item);
                        if(filtered.length > 0){
                            otherExpr.push(ECall(
                                EField(EConst(CIdent(varName)).toExpr(), "push", Normal).toExpr(),
                                [EConst(CIdent(filtered.first().name)).toExpr()]
                            ).toExpr());
                            continue;
                        }
                        var newVarName = this.nameBuilder.next();
                        otherExpr = otherExpr.concat(this.getExpr(item, newVarName.get(), ofType, added, '$stack[Arr]'));
                        otherExpr.push(ECall(
                            EField(EConst(CIdent(varName)).toExpr(), "push", Normal).toExpr(),
                            [newVarName.getIdentifier().toExpr()]
                        ).toExpr());
                    }
                case TypeList(ofType):
                    var arrayVal : List<Dynamic> = cast val;
                    otherExpr.push(EBinop(OpAssign, EConst(CIdent(varName)).toExpr(), macro new List()).toExpr());
                    for(item in arrayVal){
                        filtered = added.filter(it->it.val==item);
                        if(filtered.length > 0){
                            otherExpr.push(ECall(
                                EField(EConst(CIdent(varName)).toExpr(), "add", Normal).toExpr(),
                                [EConst(CIdent(filtered.first().name)).toExpr()]
                            ).toExpr());
                            continue;
                        }
                        var newVarName = this.nameBuilder.next();
                        otherExpr = otherExpr.concat(this.getExpr(item, newVarName.get(), ofType, added, '$stack[List]'));
                        otherExpr.push(ECall(
                            EField(EConst(CIdent(varName)).toExpr(), "add", Normal).toExpr(),
                            [newVarName.getIdentifier().toExpr()]
                        ).toExpr());
                    }
                case TypeMap(ofKeyType, ofValueType):
                    otherExpr.push(EBinop(OpAssign, EConst(CIdent(varName)).toExpr(), macro []).toExpr());
                    var arrayVal : Map<Dynamic,Dynamic> = cast val;
                    var values : Array<Expr> = [];
                    for(key => item in arrayVal){
                        var varKeyNameExpr : Expr;
                        filtered = added.filter(it->it.val==key);
                        if(filtered.length > 0){
                            varKeyNameExpr = EConst(CIdent(filtered.first().name)).toExpr();
                        } else {
                            var newVarNameKey = this.nameBuilder.next();
                            otherExpr = otherExpr.concat(this.getExpr(key,  newVarNameKey.get(), ofKeyType, added, '$stack[Map.Key]'));
                            varKeyNameExpr = newVarNameKey.getIdentifier().toExpr();
                        }
                        var varVarNameExpr : Expr;
                        filtered = added.filter(it->it.val==item);
                        if(filtered.length > 0){
                            varVarNameExpr = EConst(CIdent(filtered.first().name)).toExpr();
                        } else {
                            var newVarNameVal = this.nameBuilder.next();
                            otherExpr = otherExpr.concat(this.getExpr(item, newVarNameVal.get(), ofValueType, added, '$stack[Map.Value]'));
                            varVarNameExpr = newVarNameVal.getIdentifier().toExpr();
                        }
                        otherExpr.push(macro $i{varName}[$varKeyNameExpr] = $varVarNameExpr);
                    }
                case TypeStruct(fields):
                    otherExpr.push(EBinop(OpAssign, EConst(CIdent(varName)).toExpr(), macro cast {}).toExpr());
                    var varNameExpr : Expr;
                    for(field in fields){
                        filtered = added.filter(it->it.val==val.field(field.name));
                        if(filtered.length > 0){
                            varNameExpr = EConst(CIdent(filtered.first().name)).toExpr();
                        } else {
                            var newVarName = this.nameBuilder.next();
                            otherExpr = otherExpr.concat(this.getExpr(val.field(field.name), newVarName.get(), field, added, '$stack.${field.name}'));
                            varNameExpr = newVarName.getIdentifier().toExpr();
                        }
                        otherExpr.push(EBinop(OpAssign, EField(EConst(CIdent(varName)).toExpr(), field.name).toExpr(), varNameExpr).toExpr());
                    }
            }
        } else if (type.nullable){
            trace(varName, null);
            otherExpr.push(EVars([{
                name: varName,
                type: type.realType.toComplexType(),
                expr: macro null,
            }]).toExpr());
        } else {
            throw 'Non-Null-Value ($stack) is null: ${type.decoded.getName()}';
        }
        return otherExpr;
    }

    public static function saveToTypedVariable(val: Dynamic, varName: String, type: ComplexType) : Array<Expr> {
        return new MacroValueSaverExpr(varName).getExpr(val, varName, type.toType().decode());
    }

    public static function saveToVariable(val: Dynamic, varName: String, ?type_opt: haxe.macro.Type) : Array<Expr> {
        var type : haxe.macro.Type = if(type_opt != null) type_opt else val.valToType();
        return new MacroValueSaverExpr(varName).getExpr(val, varName, type.decode());
    }
    #end
}
#end
