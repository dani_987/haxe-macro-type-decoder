# Haxe Macro-Types Helper Functions

Some functions to make life easier when working with types and macros.

## Install

```
haxelib install macro-type-decoder
```

## Usage

### Decoding a Type, to process inside a macro

```
#if macro
using hx.macro.MacroTypeDecorderHelper;
//...
var someType : Type = //...
var decodedType : hx.macro.MacroTypeDecorderHelper.DecodedType = someType.decode();
//...
#end
```

### Using some helper functions inside macros

#### Creating an Expr from variable-Name stored in an String-Variable

```
#if macro
using hx.macro.MacroDefineHelper;
//...
var identName : String = //...
EConst(CIdent(identName)).toExpr()
//...
#end
```

#### Defining some functions in build macro, that are returning specific constant values

```
#if macro
using hx.macro.MacroDefineHelper;
//...
static function build() : ComplexType {
    var newName : String = //...
    var pack : Array<String> = //...
    if(!isDefinedAsTypeInPath(name, pack)){
        Context.defineType({
            name: newName,
            pack: pack,
            pos: MacroDefineHelper.getPosition(),
            params: [],
            kind: TDClass(),
            fields: [
                "new".toPublicFunction([], macro {}, macro: Void),
                "getSomeMap".toPublicConstValueFunction(
                    [
                        "hi" => 5,
                        "hallo" => 7,
                    ], macro : Map<String,Int>
                ),
                //other fields
            ],
        });
    }
    return TPath({pack: pack, name: newName, params: []});
}
//...
#end
```

### Getting some Type-Information at runtime

```
var intInfo = new hx.macro.TypeToStringPrinter<Int>();
intInfo.getString(); // Int
intInfo.getDecodedData(); // {nullable: false, decoded: TypeInt}
```

### Other examples

To see other examples see the tests and look into the sources.
